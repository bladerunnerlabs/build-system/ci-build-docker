# Copyright (c) BladeRunner Labs

FROM python:slim

LABEL maintainer="BladeRunner Labs <alexander@bladerunner.io>"

USER root

RUN apt update \
 && DEBIAN_FRONTEND=noninteractive apt-get install \
    -yq --no-install-recommends \
    apt-utils \
    gcc \
    make \
    ninja-build \
    cmake \
    tar \
    vim-tiny \
 && DEBIAN_FRONTEND=noninteractive apt-get clean \
 && rm -rf /var/lib/apt/lists/*

CMD /bin/bash
