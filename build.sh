#!/bin/bash

IMG_REPO="bladerunnerlabs"
IMG_NAME="ci-builder"
IMG_TAG="0.1"
NO_CACHE= #--no-cache

[[ "$1" == "-p" || "$1" == "--push" ]] && PUSH=true

function err_exit() {
    echo "Error exit..."
    exit 1
}

echo "Build ${img_name} ..."
docker build ${NO_CACHE} \
    -t "${IMG_NAME}:${IMG_TAG}" \
    -t "${IMG_NAME}:latest" \
    -f Dockerfile . || err_exit

if [ -n "${PUSH}" ]; then
    docker tag "${IMG_NAME}:${IMG_TAG}" "${IMG_REPO}/${IMG_NAME}:${IMG_TAG}"
    echo "docker push ${IMG_REPO}/${IMG_NAME}:${IMG_TAG}"
    docker push "${IMG_REPO}/${IMG_NAME}:${IMG_TAG}" || err_exit

    docker tag "${IMG_NAME}:latest" "${IMG_REPO}/${IMG_NAME}:latest"
    echo "docker push ${IMG_REPO}/${IMG_NAME}:latest"
    docker push "${IMG_REPO}/${IMG_NAME}:latest" || err_exit
fi
